angular.module('mod.m40', []);

angular.module('mod.m40')
  .constant('MOD_MyMod2', {
        API_URL: '',
        API_URL_DEV: ''
    })
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('m40', {
                url: '/MyMod2',
                parent: 'layout',
                template: "<div ui-view></div>",
                data: {
                    type: 'home'
                }
            })
            .state('m40.header', {
               url: '',
               template: "<div></div>",
               data: {
                   templateUrl:'mod_MyMod2/views/menu/MyMod2.header.html',
                   position: 1,
                   type: 'header',
                   name: "Profile",
                   module: "MyMod2"
               }
           })
            .state('m40.dashboard', {
                url: '/dashboard',
                templateUrl: "mod_MyMod2/views/dashboard/MyMod2.dashboard.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Dashboard",
                    module: "MyMod2"
                }

            })

            //FOR APP ADMIN
           .state('m40.admin', {
               url: '/admin',
               templateUrl: "mod_MyMod2/views/admin/MyMod2.admin.html",
               data: {
                   type: 'home',
                   menu: true,
                   admin : true,
                   disabled : false,
                   name: "Admin Page",
                   module: "MyMod2"
               }
           })

    }])